package com.nhan4u.controller;

import com.nhan4u.model.*;
import com.nhan4u.service.BillService;
import com.nhan4u.tool.SendEmail;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@WebServlet("/CheckoutServlet")
public class CheckoutServlet extends HttpServlet {
	BillService service = new BillService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=utf8mb4");
		req.setCharacterEncoding("utf-8");
		// TODO Auto-generated method stub
		String payment = req.getParameter("payment");
		String address = req.getParameter("address");

		System.out.println(address + "Validate address Filter");
		String phone = req.getParameter("phone");
		HttpSession session = req.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		User user = (User) session.getAttribute("user");
		try {
			long Id = new Date().getTime();
			Bill bill = new Bill();
			bill.setBillId(Id);
			bill.setAddress(address);
			bill.setPayment(payment);
			bill.setUser(user);
			bill.setPhone(phone);
			bill.setStatus(-1);
			bill.setDate(new Timestamp(new Date().getTime()));
			bill.setTotal(cart.totalCart());
			Set<BillDetail> details = new HashSet<BillDetail>();
			int i = cart.getItems().size();
			for (Map.Entry<Long, Item> item : cart.getItems().entrySet()) {
				BillDetail detail = new BillDetail();
				detail.setPrice(item.getValue().getProduct().getPrice() * item.getValue().getQuantity());
				Product product = new Product();
				product.setId(item.getValue().getProduct().getId());
				detail.setProduct(product);
				detail.setQuantity(item.getValue().getQuantity());
				detail.setBillDetailId(new Date().getTime() + i++);
				details.add(detail);
			}
			bill.setBillDetails(details);
			service.insertBill(bill);
			new SendEmail().sendMail(user.getEmail(), "Xac nhan don hang", "Vui long xac nhan don hang giup em ak!");
			cart = new Cart();
			session.setAttribute("cart", cart);
		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.sendRedirect("index.jsp");
	}

}
