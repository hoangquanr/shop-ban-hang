package com.nhan4u.filter;

import com.nhan4u.tool.Utils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter({ "/CheckoutServlet" })
public class ValidateCheckoutFiler implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String address = request.getParameter("address");
		String phone = request.getParameter("phone");
		System.out.println(address + "Validate address Filter");
		String error = "", url = "checkout.jsp";
		if (address == null || phone == null || address.equals("") || phone.equals("")) {
			error = "Địa chỉ không đúng";
		}
		if (!Utils.checkNumberPhone(phone)) {
			if (error.equals(""))
				error = "Số điện thoại không đúng";
			else
				error += ", Số điện thoại không đúng";
		}
		request.setAttribute("error", error);
		if (error.equals("")) {
			chain.doFilter(request, response);
		} else {
			req.getRequestDispatcher(url).forward(req, resp);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

}
