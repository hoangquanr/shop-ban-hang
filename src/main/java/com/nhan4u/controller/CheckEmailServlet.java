package com.nhan4u.controller;

import com.nhan4u.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/CheckEmailServlet")
public class CheckEmailServlet extends HttpServlet{
	
	UserService service = new UserService();
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (service.checkEmailAddress(req.getParameter("email"))) {
	          resp.getWriter().write("<img src=\"images/register/not-availabel.png\" />");
	     } else {
	          resp.getWriter().write("<img src=\"images/register/available.png\" />");
	     }
	}

}
