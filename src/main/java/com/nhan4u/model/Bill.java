package com.nhan4u.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name="bill")
public class Bill {
	@Id
	@Column(name="bill_id")
	private long billId;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	@Column(name="total")
	private double total;
	@Column(name="payment")
	private String payment;
	@Column(name="address")
	private String address;
	@Column(name="date")
	private Timestamp date;
	@Column(name="status")
	private int status;
	@Column(name="phone")
	private String phone;
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@OneToMany(cascade= CascadeType.ALL)
    @JoinColumn(name="bill_id")
    private Set<BillDetail> billDetails;
	
	public Bill() {
		
	}

	public long getBillId() {
		return billId;
	}

	public void setBillId(long billId) {
		this.billId = billId;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public Set<BillDetail> getBillDetails() {
		return billDetails;
	}

	public void setBillDetails(Set<BillDetail> bills) {
		this.billDetails = bills;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Bill(long billId, User user, double total, String payment, String address, Timestamp date, int status,
			String phone, Set<BillDetail> billDetails) {
		super();
		this.billId = billId;
		this.user = user;
		this.total = total;
		this.payment = payment;
		this.address = address;
		this.date = date;
		this.status = status;
		this.phone = phone;
		this.billDetails = billDetails;
	}

	
}
