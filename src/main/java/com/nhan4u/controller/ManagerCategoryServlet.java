package com.nhan4u.controller;

import com.nhan4u.model.Category;
import com.nhan4u.service.CategoryService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/ManagerCategoryServlet")
public class ManagerCategoryServlet extends HttpServlet {

	CategoryService service = new CategoryService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		String command = req.getParameter("command");
		String categoryId = req.getParameter("categoryId");

		String url = "";
		try {

			
			switch (command) {
			
			case "delete":
				Category category = new Category();
				category.setCategoryId(Long.parseLong(categoryId));
				service.delete(category);
				url = "admin/manager-category.jsp";
				break;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.sendRedirect(url);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		String command = req.getParameter("command");
		String tenDanhMuc = req.getParameter("tenDanhMuc");
		System.out.println(tenDanhMuc);
		String categoryId = req.getParameter("categoryId");
		String url = "";
		try {
			Category category = new Category();
			switch (command) {
			case "insert":
				
				category.setCategoryId(new Date().getTime());
				category.setCategoryName(tenDanhMuc);
				service.insert(category);
				url = "admin/manager-category.jsp";
				break;
			case "update":
				category.setCategoryId(Long.parseLong(categoryId));
				category.setCategoryName(tenDanhMuc);
				service.update(category);
				url = "admin/manager-category.jsp";
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.sendRedirect(url);
	}

}
