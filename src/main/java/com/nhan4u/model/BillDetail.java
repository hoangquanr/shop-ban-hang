package com.nhan4u.model;

import javax.persistence.*;

@Entity
@Table(name="bill_detail")
public class BillDetail {
	
	@Id
	@Column(name="bill_detail_id")
	private long billDetailId;
	
	@ManyToOne
	@JoinColumn(name="bill_id")
	private Bill bill;
	@ManyToOne
	@JoinColumn(name="product_id")
	private Product product;
	@Column(name="price")
	private double price;
	@Column(name="quantity")
	private int quantity;
	
	public BillDetail() {
		
	}

	public long getBillDetailId() {
		return billDetailId;
	}

	public void setBillDetailId(long billDetailId) {
		this.billDetailId = billDetailId;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BillDetail(long billDetailId, Bill bill, Product product, double price, int quantity) {
		super();
		this.billDetailId = billDetailId;
		this.bill = bill;
		this.product = product;
		this.price = price;
		this.quantity = quantity;
	}


}
