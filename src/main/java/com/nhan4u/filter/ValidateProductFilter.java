package com.nhan4u.filter;

import com.nhan4u.service.ProductService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet Filter implementation class ValidateProductFilter
 */
@WebFilter("/ManagerProductServlet")
public class ValidateProductFilter implements Filter {

	ProductService service = new ProductService();

	/**
	 * Default constructor.
	 */
	public ValidateProductFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpServletResponse resp = (HttpServletResponse) response;
		String command = request.getParameter("command");
		String productId = request.getParameter("productId");
		String tenSanPham = request.getParameter("tenSanPham");
		String price = request.getParameter("giaSanPham");
		String url = "", error = "";
		if (command.equals("delete"))
			chain.doFilter(request, response);
		// Không được để trống
		else {
			if (tenSanPham.equals("") || price.equals("")) {
				error = "Vui long nhập thông tin!";
			} else if (service.checkProduct(tenSanPham))
				error = "Trùng tên sản phẩm";
			if (price.equals("")) {
				if (error.equals(""))
					error = "Vui long nhập thông tin!";
				error += ", Vui long nhập thông tin!";
			}
			request.setAttribute("errorProduct", error);
			if (error.equals(""))
				chain.doFilter(request, response);
			else {
				if (command.equals("update"))
					resp.sendRedirect("admin/update-product.jsp?productId=" + productId);
				else
					resp.sendRedirect("admin/insert-product.jsp");
			}
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
