<%@page import="com.nhan4u.model.BillDetail"%>
<%@page import="com.nhan4u.service.BillDetailService"%>
<%@page import="com.nhan4u.model.Bill"%>
<%@page import="java.util.List"%>
<%@page import="com.nhan4u.service.BillService"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Java Server Page</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<%
		long billId = Long.parseLong(request.getParameter("billId"));
		BillService billService = new BillService();
		Bill bill = billService.getBillById(billId);
		BillDetailService detailsService = new BillDetailService();
		List<BillDetail> details = detailsService.getBillDetailsByBillId(billId);
	%>
	<div id="wrapper">
		<jsp:include page="jsp/nav.jsp"></jsp:include>
		<div id="page-wrapper">

			<div class="container-fluid">

				<div class="row">
					<h2>
						Hóa đơn của
						<%=bill.getUser().getEmail()%>
						mã hóa đơn:
						<%=billId%></h2>
					<form action="/ShopQuanAo/ManagerBillServlet?billId=<%=billId%>"
						method="post">
						<%
							if (request.getAttribute("errorBill") != null) {
						%>
						<span style="color: red"><%=request.getAttribute("errorBill")%></span>
						<%
							}
						%>
						<div class="form-group">
							<label>Địa chỉ giao hàng</label> <input class="form-control"
								name="diachi" value="<%=bill.getAddress()%>">
						</div>
						<div class="form-group">
							<label>Số điện thoại</label> <input class="form-control"
								name="phone" value="<%=bill.getPhone()%>">
						</div>
						<button type="submit" class="btn btn-default">Lưu dữ liệu</button>
				</div>
				</form>

				<div class="row">

					<h3>
						Chi tiết hóa đơn:
						<%=billId%></h3>

					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
								<tr>
									<th>STT</th>
									<th>Mã</th>
									<th>Sản phẩm</th>
									<th>Số lượng</th>
									<th>Tổng tiền</th>
									<th>Tùy chọn</th>
								</tr>
							</thead>
							<tbody>
								<%
									int count = 0;
									for (BillDetail detail : details) {
										count++;
								%>
								<tr>
									<td><%=count%></td>
									<td><%=detail.getBillDetailId()%></td>
									<td><%=detail.getProduct().getName()%></td>
									<td><%=detail.getQuantity()%></td>
									<td><%=detail.getPrice()%></td>
									<td>
										<center>
											<a
												href="/ShopQuanAo/ManagerBillDetailServlet?command=plus&billDetailId=<%=detail.getBillDetailId()%>">Tăng</a>
											| <a
												href="/ShopQuanAo/ManagerBillDetailServlet?command=subtract&billDetailId=<%=detail.getBillDetailId()%>">Giảm</a>
											| <a
												href="/ShopQuanAo/ManagerBillDetailServlet?command=delete&billDetailId=<%=detail.getBillDetailId()%>">Xóa</a>
										</center>

									</td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>
					</div>

				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Morris Charts JavaScript -->
	<script src="js/plugins/morris/raphael.min.js"></script>
	<script src="js/plugins/morris/morris.min.js"></script>
	<script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>