<%@page import="com.nhan4u.model.User"%>
<%@page pageEncoding="utf-8"%>
<%
	User user = (User) session.getAttribute("useradmin");
%>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.jsp">Quản lý cửa hàng</a>
	</div>
	<!-- Top Menu Items -->
	<ul class="nav navbar-right top-nav">
		<li class="dropdown"><a href="#" class="dropdown-toggle"
			data-toggle="dropdown"><i class="fa fa-user"></i> <%
 	if (user != null) {
 %><%=user.getEmail()%> <%
 	}
 %> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a href="#"><i class="fa fa-fw fa-gear"></i> Change password</a></li>
				<li><a
					href="/ShopQuanAo/LogoutServlet?logout=true&command=admin"><i
						class="fa fa-fw fa-power-off"></i> Log Out</a></li>
			</ul></li>

	</ul>
	<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav side-nav">
			<li
				class="${pageContext.request.requestURI eq '/ShopQuanAo/admin/index.jsp' ? ' active' : ''}"><a
				href="index.jsp"><i class="fa fa-fw fa-dashboard"></i> Trang chủ</a></li>
			<li
				class="${pageContext.request.requestURI eq '/ShopQuanAo/admin/manager-category.jsp' ? ' active' : ''}"><a
				href="manager-category.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>
					Danh mục</a></li>
			<li
				class="${pageContext.request.requestURI eq '/ShopQuanAo/admin/manager-product.jsp' ? ' active' : ''}"><a
				href="manager-product.jsp"><i class="fa fa-fw fa-table"></i> Sản
					phẩm</a></li>
			<li
				class="${pageContext.request.requestURI eq '/ShopQuanAo/admin/manager-bill.jsp' ? ' active' : ''}"><a
				href="manager-bill.jsp"><i class="fa fa-fw fa-edit"></i> Hóa đơn</a></li>

		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>
