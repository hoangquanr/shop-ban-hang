package com.nhan4u.filter;

import com.nhan4u.service.CategoryService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet Filter implementation class ValidateCategoryFilter
 */
@WebFilter("/ManagerCategoryServlet")
public class ValidateCategoryFilter implements Filter {

	CategoryService service = new CategoryService();

	/**
	 * Default constructor.
	 */
	public ValidateCategoryFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpServletResponse resp = (HttpServletResponse) response;
		String command = request.getParameter("command");
		String tenDanhMuc = request.getParameter("tenDanhMuc");
		String categoryId = request.getParameter("categoryId");
		String url = "", error = "";
		if (command.equals("delete"))
			chain.doFilter(request, response);
		else {
			if (tenDanhMuc.equals("")) {
				error = "Vui long nhập tên danh mục!";
			}
			if (service.checkName(tenDanhMuc))
				error = "Trùng tên danh mục";
			request.setAttribute("errorCategory", error);
			if (error.equals(""))
				chain.doFilter(request, response);
			else {
				if (command.equals("update"))
					resp.sendRedirect("admin/update-category.jsp?categoryId=" + categoryId);
				else
					resp.sendRedirect("admin/insert-category.jsp");
			}
		}

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
