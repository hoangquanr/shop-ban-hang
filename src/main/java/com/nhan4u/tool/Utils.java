package com.nhan4u.tool;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	public static String getCutName(String name) {
		if (name.length() > 200)
			name = name.substring(0, 199) + "...";

		return name;
	}

	public static String getShortName(String name) {
		if (name.length() > 180)
			name = name.substring(0, 179) + "...";

		return name;
	}

	public static boolean checkNumberPhone(String number) {
		Pattern pattern = Pattern.compile("^[0-9]*$");
		Matcher matcher = pattern.matcher(number);
		if (!matcher.matches()) {
			return false;
		} else if (number.length() == 10 || number.length() == 11) {
			if (number.length() == 10) {
				if (number.substring(0, 2).equals("09") || number.substring(0, 2).equals("03")) {
					return true;
				} else {
					return false;
				}
			} else if (number.substring(0, 2).equals("01")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static boolean checkMoney(String chuoi) {
		try {
			int giatri = Integer.parseInt(chuoi);
			if (giatri % 100 == 0)
				return true;
			return false;
		} catch (Exception e) {
			return false;
		}
	}

}
