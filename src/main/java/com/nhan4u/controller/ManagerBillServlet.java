package com.nhan4u.controller;

import com.nhan4u.model.Bill;
import com.nhan4u.service.BillService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ManagerBillServlet")
public class ManagerBillServlet extends HttpServlet {

	BillService service = new BillService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String command = req.getParameter("command");
		String billId = req.getParameter("billId");
		Bill bill = service.getBillById(Long.parseLong(billId));
		switch (command) {
		case "done":
			bill.setStatus(0);
			service.update(bill);
		case "cancel":
			bill.setStatus(1);
			service.update(bill);
		}
		resp.sendRedirect("admin/manager-bill.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String billId = req.getParameter("billId");
		String diachi = req.getParameter("diachi");
		String phone = req.getParameter("phone");
		Bill bill = service.getBillById(Long.parseLong(billId));
		bill.setAddress(diachi);
		bill.setPhone(phone);
		service.update(bill);
		resp.sendRedirect("admin/update-bill.jsp?billId=" + billId);

	}

}
