<%@page import="com.nhan4u.filter.CountFilter"%>
<%@page import="com.nhan4u.model.BillDetail"%>
<%@page import="java.util.List"%>
<%@page import="com.nhan4u.service.BillDetailService"%>
<%@page import="com.nhan4u.model.Bill"%>
<%@page import="java.util.Date"%>
<%@page import="com.nhan4u.service.BillService"%>
<%@page import="com.nhan4u.model.User"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf8mb4">
<title>Java Server Page</title>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<%
		BillService billService = new BillService();
		double total = billService.total();
		long totalNewBill = billService.totalNewBill();
		List<Bill> bills = billService.getListOfNewBill();
		long totalSell = (new BillDetailService()).totalSell();
	%>
	<div id="wrapper">
		<jsp:include page="jsp/nav.jsp"></jsp:include>
		<div id="page-wrapper">

			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Thống kê</h1>
					</div>
				</div>
				<!-- /.row -->

				<!-- /.row -->

				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-comments fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div style="font-size: 22px"><%=total%>VNĐ
										</div>
										<div>
											Thu nhập tháng
											<%=new Date().getMonth() + 1%></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-green">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-tasks fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div style="font-size: 22px"><%=CountFilter.count %></div>
										<div>Số lượng truy cập</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-yellow">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-shopping-cart fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div style="font-size: 22px"><%=totalNewBill %></div>
										<div>Đơn hàng mới</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-red">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-support fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div style="font-size: 22px"><%=totalSell %></div>
										<div>Sản phẩm đã bán</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">
								<i class="fa fa-money fa-fw"></i> Đơn mới đặt
							</h3>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th>STT</th>
											<th>Mã hóa đơn</th>
											<th>Khách hàng</th>
											<th>Tổng hóa đơn</th>
											<th>Địa chỉ giao hàng</th>
											<th>Số điện thoại</th>
											<th>Ngày mua</th>
											<th>Hình thức thanh toán</th>
											<th>Chi tiết hóa đơn</th>
											<th>Tùy chọn</th>
										</tr>
									</thead>
									<tbody>
										<%
											int count = 0;
											for (Bill bill : bills) {
												count++;
										%>
										<tr <%if (bill.getStatus() == 0) {%> class="success" <%}%>
											<%if (bill.getStatus() == 1) {%> class="danger" <%}%>>
											<td><%=count%></td>
											<td><%=bill.getBillId()%></td>
											<td><%=bill.getUser().getEmail()%></td>
											<td><%=bill.getTotal()%></td>
											<td><%=bill.getAddress()%></td>
											<td><%=bill.getPhone()%></td>
											<td><%=bill.getDate()%></td>
											<td><%=bill.getPayment()%></td>
											<td><a
												href="/ShopQuanAo/admin/update-bill.jsp?billId=<%=bill.getBillId()%>">Chi
													tiết</a></td>
											<td>
												<%
													if (bill.getStatus() == -1) {
												%>
												<center>
													<a
														href="/ShopQuanAo/ManagerBillServlet?command=done&billId=<%=bill.getBillId()%>">Xác
														nhận</a> | <a
														href="/ShopQuanAo/ManagerBillServlet?command=cancel&billId=<%=bill.getBillId()%>">Hủy</a>


												</center> <%
 	}
 %>

											</td>
										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<div class="text-right">
								<a href="#">View All Transactions <i
									class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->


			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Morris Charts JavaScript -->
	<script src="js/plugins/morris/raphael.min.js"></script>
	<script src="js/plugins/morris/morris.min.js"></script>
	<script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>