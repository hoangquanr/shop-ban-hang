<%@page import="com.nhan4u.model.Category"%>
<%@page import="com.nhan4u.service.CategoryService"%>
<%@page import="com.nhan4u.model.Brand"%>
<%@page import="java.util.List"%>
<%@page import="com.nhan4u.service.BrandService"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Quản lý sản phẩm</title>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<%
		BrandService brandService = new BrandService();
		List<Brand> brands = brandService.getListOfBrands();
		CategoryService categoryService = new CategoryService();
		List<Category> categories = categoryService.getListOfCategories();
	%>
	<div id="wrapper">
		<jsp:include page="jsp/nav.jsp"></jsp:include>
		<div id="page-wrapper">

			<div class="container-fluid">


				<center>
					<form method="post" action="/ShopQuanAo/UploadServlet"
						enctype="multipart/form-data">
						Select file to upload: <input type="file" name="uploadFile" /> <br />
						<br /> <input type="submit" value="Upload" />
					</form>
				</center>
				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Thêm sản phẩm</h1>
						<ol class="breadcrumb">
							<li><i class="fa fa-dashboard"></i> <a href="index.html">Dashboard</a>
							</li>
							<li class="active"><i class="fa fa-edit"></i> Forms</li>
						</ol>
					</div>
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-lg-6">

						<form role="form" action="/ShopQuanAo/ManagerProductServlet"
							method="post">
							<%
								if (request.getAttribute("errorProduct") != null) {
							%>
							<span style="color: red"><%=request.getAttribute("errorProduct")%></span>
							<%
								}
							%>
							<div class="form-group">
								<label>Tên sản phẩm</label> <input class="form-control"
									name="tenSanPham">
							</div>

							<div class="form-group">
								<label>Giá sản phẩm</label> <input class="form-control"
									name="giaSanPham">
							</div>



							<div class="form-group">
								<label>Hình ảnh</label> <input type="file" name="hinhSanPham">
							</div>

							<div class="form-group">
								<label>Mô tả</label>
								<textarea class="form-control" rows="3" name="motaSanPham"></textarea>
							</div>




							<div class="form-group">
								<label>Loại sản phẩm</label> <select class="form-control"
									name="categoryId">
									<%
										for (Category category : categories) {
									%>
									<option value="<%=category.getCategoryId()%>"><%=category.getCategoryName()%></option>
									<%
										}
									%>
								</select>
							</div>

							<div class="form-group">
								<label>Thương hiệu</label> <select class="form-control"
									name="brandId">
									<%
										for (Brand brand : brands) {
									%>
									<option value="<%=brand.getBrandId()%>"><%=brand.getBrandName()%></option>
									<%
										}
									%>
								</select>
							</div>
							<input type="hidden" name="command" value="insert" />
							<button type="submit" class="btn btn-default">Tạo sản
								phẩm</button>

						</form>

					</div>

				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Morris Charts JavaScript -->
	<script src="js/plugins/morris/raphael.min.js"></script>
	<script src="js/plugins/morris/morris.min.js"></script>
	<script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>