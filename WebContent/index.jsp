<%@page import="com.nhan4u.service.TestService"%>
<%@page import="com.nhan4u.model.Category"%>
<%@page import="java.util.List"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Java Server Page</title>
<jsp:include page="jsp/head.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="jsp/header.jsp"></jsp:include>
	<jsp:include page="jsp/slider.jsp"></jsp:include>
	<jsp:include page="jsp/content.jsp"></jsp:include>
	<jsp:include page="jsp/footer.jsp"></jsp:include>
</body>
</html>