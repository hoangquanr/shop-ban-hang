package com.nhan4u.controller;

import com.nhan4u.model.BillDetail;
import com.nhan4u.service.BillDetailService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ManagerBillDetailServlet")
public class ManagerBillDetailServlet extends HttpServlet {

	BillDetailService service = new BillDetailService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String command = req.getParameter("command");
		String billDetailId = req.getParameter("billDetailId");
		BillDetail bd = service.getBillDetailById(Long.parseLong(billDetailId));
		long billId = bd.getBill().getBillId();
		switch (command) {
		case "delete":
			service.delete(bd);
			break;
		case "plus":
			int quantity = bd.getQuantity() + 1;
			bd.setQuantity(quantity);
			service.update(bd);
			break;
		case "subtract":
			int soluong = bd.getQuantity();
			if (soluong == 1) {
				service.delete(bd);
			} else {
				bd.setQuantity(bd.getQuantity() - 1);
				service.update(bd);
			}
			break;
		}
		resp.sendRedirect("admin/update-bill.jsp?billId=" + billId);
	}

}
