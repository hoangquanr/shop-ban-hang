package com.nhan4u.service;

import com.nhan4u.hibernate.util.HibernateUtil;
import com.nhan4u.model.Brand;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class BrandService {
	
public List<Brand> getListOfBrands(){
		
		List<Brand> list = new ArrayList<Brand>();
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			list = session.createQuery("from Brand").list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}


}
