package com.nhan4u.model;

import java.util.HashMap;
import java.util.Map;

public class Cart {

    private HashMap<Long,Item> items;

	public HashMap<Long, Item> getItems() {
		return items;
	}

	public void setItems(HashMap<Long, Item> items) {
		this.items = items;
	}

	public Cart(HashMap<Long, Item> items) {
		super();
		this.items = items;
	}
	
	public Cart() {
		items = new HashMap<Long, Item>();
	}
	
	public void insertToCart(Long key, Item item) {
		boolean check = items.containsKey(key);
		if(check) {// hois ssai sai
			int quantityOld = item.getQuantity();
			item.setQuantity(quantityOld + 1);
			items.put(key, item);
		}
		else
			items.put(key, item);
	}
	
	public void subToCart(Long key, Item item) {
		boolean check = items.containsKey(key);
		if(check) {// hois ssai sai
			int quantityOld = item.getQuantity();
			if(quantityOld <= 1)
				items.remove(key);
			else {
				item.setQuantity(quantityOld - 1);
				items.put(key, item);
			}
		}
	}
	
	public void removeToCart(Long key) {
		boolean check = items.containsKey(key);
		if(check) {// hois ssai sai
			items.remove(key);
		}
	}
	
	public int countItem() {
		return items.size();
	}
	
	public double totalCart() {
		double total = 0.0;
		for(Map.Entry<Long, Item> item : items.entrySet()) {
			total += item.getValue().getProduct().getPrice() * item.getValue().getQuantity();
		}
		return total;
	}
	
	
}
