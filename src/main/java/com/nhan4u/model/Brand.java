package com.nhan4u.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="brand")
public class Brand {
	
	@Id
	@Column(name="brand_id")
	private long brandId;
	@Column(name="brand_image")
	private String brandImage;
	@OneToMany(mappedBy = "brand", cascade = CascadeType.ALL)
	private Set<Product> products;
	public Set<Product> getProducts() {
		return products;
	}
	public void setProducts(Set<Product> products) {
		this.products = products;
	}
	public String getBrandImage() {
		return brandImage;
	}
	public void setBrandImage(String brandImage) {
		this.brandImage = brandImage;
	}
	public long getBrandId() {
		return brandId;
	}
	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	@Column(name="brand_name")
	private String brandName;
	
	public Brand() {
		
	}
	public Brand(long brandId, String brandImage, Set<Product> products, String brandName) {
		super();
		this.brandId = brandId;
		this.brandImage = brandImage;
		this.products = products;
		this.brandName = brandName;
	}
	
	

}
