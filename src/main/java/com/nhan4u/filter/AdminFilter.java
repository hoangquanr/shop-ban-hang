package com.nhan4u.filter;

import com.nhan4u.model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = { "/admin/index.jsp", "/admin/manager-bill.jsp", "/admin/manager-category.jsp",
		"/admin/manager-product.jsp", "/admin/update-bill.jsp", "/admin/update-category.jsp", "/admin/update-product.jsp",
		"/admin/insert-category.jsp", "/admin/insert-product.jsp" })
public class AdminFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("useradmin");

		if (user != null) {
			chain.doFilter(request, response);
		} else {
			resp.sendRedirect("/ShopQuanAo/admin/login.jsp");
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
