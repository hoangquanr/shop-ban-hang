package com.nhan4u.filter;

import com.nhan4u.model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({ "/login.jsp"})
public class LoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		// Neu chua dang nhap yeu cau nguoi dung dang nhap
		String url = "", error = "";
		User user = (User) session.getAttribute("user");
		// Kiểm tra nếu đã đăng nhập r thì return về index
		if (user != null)
			url = "index.jsp";
		if (url.equals(""))
			chain.doFilter(req, resp);
		else
			resp.sendRedirect(url);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

}
