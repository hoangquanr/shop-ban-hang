<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Java Server Page</title>
<jsp:include page="jsp/head.jsp"></jsp:include>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"
	type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(
			function() {
				var x_timer;
				$("#email").keyup(function(e) {
					clearTimeout(x_timer);
					var email = $(this).val();
					x_timer = setTimeout(function() {
						check_username_ajax(email);
					}, 1000);
				});

				function check_username_ajax(email) {
					$("#user-result").html(
							'<img src="images/register/ajax-loader.gif" />');
					$.post('CheckEmailServlet', {
						'email' : email
					}, function(data) {
						$("#user-result").html(data);
					});
				}
			});
</script>
</head>
<body>
	<jsp:include page="jsp/header.jsp"></jsp:include>
	<div class="container">
		<div class="account">
			<h2 class="account-in">Register</h2>
			<form action="RegisterServlet" method="post">
				<%
					if (request.getAttribute("errorRegister") != null) {
				%>
				<span style="color: red"><%=request.getAttribute("errorRegister")%></span>
				<%
					}
				%>
				<div>
					<span class="mail">Email*</span> <input type="text" name="email"
						id="email"><span id="user-result"></span>
				</div>
				<div>
					<span class="word">Password*</span> <input type="password"
						name="password">
				</div>
				<input type="submit" value="Register">
			</form>
		</div>
	</div>
	<jsp:include page="jsp/footer.jsp"></jsp:include>
</body>
</html>