package com.nhan4u.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/LoginServlet")
public class ValidateLoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String error = "", url = "";
		String command = req.getParameter("command");
		String userEmail = req.getParameter("email");
		String password = req.getParameter("password");
		if (userEmail == null || userEmail.equals(""))
			error = "Email chưa nhập";
		if (password == null || password.equals("")) {
			if (error.equals(""))
				error = "Password chưa nhập";
			else
				error += ", Password chưa nhập";
		}
		request.setAttribute("errorLogin", error);
		if (error.equals(""))
			chain.doFilter(request, response);
		else {
			switch (command) {
			case "admin":
				url = "admin/login.jsp";
				break;
			case "user":
				url = "login.jsp";
				break;
			}
			req.getRequestDispatcher(url).forward(req, resp);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

}
