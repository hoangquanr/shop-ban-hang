package com.nhan4u.controller;

import com.nhan4u.model.Cart;
import com.nhan4u.model.Item;
import com.nhan4u.model.Product;
import com.nhan4u.service.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/CartServlet")
public class CartServlet extends HttpServlet {

	ProductService service = new ProductService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		HttpSession session = req.getSession();
		String command = req.getParameter("command");
		String productId = req.getParameter("productId");
		Cart cart = (Cart) session.getAttribute("cart");
		try {
			Product product = service.getProductById(Long.parseLong(productId));
			Long idProduct = Long.parseLong(productId);
			switch (command) {
			case "plus":
				if (cart.getItems().containsKey(idProduct)) {
					cart.insertToCart(idProduct, new Item(product, cart.getItems().get(idProduct).getQuantity()));
				} else
					cart.insertToCart(idProduct, new Item(product, 1));
				break;
			case "remove":
				cart.removeToCart(idProduct);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		session.setAttribute("cart", cart);
		resp.sendRedirect("/ShopQuanAo/index.jsp");
	}
}
