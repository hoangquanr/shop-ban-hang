<%@page import="com.nhan4u.model.Brand"%>
<%@page import="com.nhan4u.service.BrandService"%>
<%@page import="com.nhan4u.model.Category"%>
<%@page import="java.util.List"%>
<%@page import="com.nhan4u.service.CategoryService"%>
<%@page import="com.nhan4u.model.Product"%>
<%@page import="com.nhan4u.service.ProductService"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Java Server Page</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 


</script>
<style>
#more {
	display: none;
}
</style>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<link rel="stylesheet" href="css/etalage.css">
<script src="js/jquery.etalage.min.js"></script>
<script>
	jQuery(document)
			.ready(
					function($) {

						$('#etalage')
								.etalage(
										{
											thumb_image_width : 300,
											thumb_image_height : 400,
											source_image_width : 900,
											source_image_height : 1200,
											show_hint : true,
											click_callback : function(
													image_anchor, instance_id) {
												alert('Callback example:\nYou clicked on an image with the anchor: "'
														+ image_anchor
														+ '"\n(in Etalage instance: "'
														+ instance_id + '")');
											}
										});

					});
</script>
<script>
	$(document).ready(function(c) {
		$('.alert-close').on('click', function(c) {
			$('.message').fadeOut('slow', function(c) {
				$('.message').remove();
			});
		});
	});
</script>
<script>
	$(document).ready(function(c) {
		$('.alert-close1').on('click', function(c) {
			$('.message1').fadeOut('slow', function(c) {
				$('.message1').remove();
			});
		});
	});
</script>
</head>
<body>

	<%
		ProductService productService = new ProductService();
		CategoryService categoryService = new CategoryService();
		BrandService brandService = new BrandService();
		List<Brand> brands = brandService.getListOfBrands();
		List<Category> categories = categoryService.getListOfCategories();
		String product_id = "";
		Product product = null;
		if (request.getParameter("productId") != null) {
			product_id = request.getParameter("productId");
			product = productService.getProductById(Long.valueOf(product_id));
		}
		List<Product> productsOfCategory = productService.getProductsByCategory(product.getCategory().getCategoryId());
	%>
	<jsp:include page="jsp/header.jsp"></jsp:include>
	<!---->
	<div class="container">
		<div class="single">
			<div class="col-md-9 top-in-single">
				<div class="col-md-5 single-top">
					<ul id="etalage">
						<li><a href="optionallink.html"> <img
								class="etalage_thumb_image img-responsive"
								src="<%=product.getImage()%>" alt="<%=product.getName()%>">
								<img class="etalage_source_image img-responsive"
								src="<%=product.getImage()%>" alt="">
						</a></li>
						<%
							boolean first = true;
							for (String imgLink : product.getListImages()) {
								if (first) {
									first = false;
									continue;
								}
						%>
						<li><img class="etalage_thumb_image img-responsive"
							src="<%=imgLink%>" alt=""> <img
							class="etalage_source_image img-responsive" src="<%=imgLink%>"
							alt=""></li>
						<%
							}
						%>
					</ul>

				</div>
				<div class="col-md-7 single-top-in">
					<div class="single-para">
						<h4><%=product.getName()%></h4>
						<div class="para-grid">
							<span class="add-to"><%=product.getPrice()%>VND</span> <a
								href="CartServlet?command=plus&productId=<%=product.getId()%>"
								class="hvr-shutter-in-vertical cart-to">Mua ngay</a>
							<div class="clearfix"></div>
						</div>
						<h5>Con hang</h5>
						<%
							if (product.getDescription().length() > 200) {
						%>
						<p id="descriptionInfo"><%=product.getDescription().substring(0, 199)%>
							<span id="dots">...</span><span id="more"><%=product.getDescription().substring(200, product.getDescription().length() - 1)%></span>
						</p>
						<%
							}
						%>
						<button onClick="more()" id="myBtn">Xem them</button>
					</div>

				</div>
				<div>
					<div class="fb-comments"
						data-href="http://localhost:1997/ShopQuanAo/product.jsp?productId=<%=product.getId()%>"
						data-width="700" data-numposts="5"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-3">
				<div class="single-bottom">
					<h4>Dòng sản phẩm của shop</h4>
					<ul>
						<%
							for (Category category : categories) {
						%>
						<li><a href="products.jsp?categoryId=<%=category.getCategoryId()%>&pages=1"><i> </i><%=category.getCategoryName()%></a></li>
						<%
							}
						%>
					</ul>
				</div>
				<div class="single-bottom">
					<h4>Thương hiệu</h4>
					<ul>
						<%
							for (Brand brand : brands) {
						%>
						<li><a href="#%>&pages=1"><i> </i><%=brand.getBrandName()%></a></li>
						<%
							}
						%>
					</ul>
				</div>
				<div class="single-bottom">
					<h4>Sản phẩm cùng loại</h4>
					<%
						for (Product item : productsOfCategory) {
					%>
					<div class="product">
						<img class="img-responsive fashion" src="<%=item.getImage()%>"
							alt="<%=item.getName()%>" width="100" height="100">
						<div class="grid-product">
							<a href="#" class="elit"><%=item.getName()%></a> <span
								class="price price-in"><small>200,000VND</small> <%=item.getPrice()%>VND</span>
						</div>
						<div class="clearfix"></div>
					</div>
					<%
						}
					%>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<jsp:include page="jsp/footer.jsp"></jsp:include>
	<script>
		function more() {
			var dots = document.getElementById("dots");
			var moreText = document.getElementById("more");
			var btnText = document.getElementById("myBtn");

			if (dots.style.display === "none") {
				dots.style.display = "inline";
				btnText.innerHTML = "Xem them";
				moreText.style.display = "none";
			} else {
				dots.style.display = "none";
				btnText.innerHTML = "An di";
				moreText.style.display = "inline";
			}
		}
	</script>
</body>
</html>
