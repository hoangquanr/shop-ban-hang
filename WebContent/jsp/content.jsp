<!---->
<%@page import="com.nhan4u.service.BrandService"%>
<%@page import="com.nhan4u.model.Brand"%>
<%@page import="com.nhan4u.tool.Utils"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.nhan4u.model.Cart"%>
<%@page import="com.nhan4u.model.Product"%>
<%@page import="com.nhan4u.service.ProductService"%>
<%@ page pageEncoding="utf-8"%>
<%
	ProductService service = new ProductService();
	List<Product> productsBestSeller = service.getProductsBestSeller();
	List<Product> productsHot = service.getProductsBestSeller();
	List<Product> products = service.getProducts();
	BrandService brandService = new BrandService();
	List<Brand> brands = brandService.getListOfBrands();
%>
<div class="container">
	<div class="content">

		<div class="container">
			<div class="products">
				<h2 class=" products-in">SẢN PHẨM BÁN CHẠY</h2>
				<%
					int indexProduct = 0;
					int count = 0;
					for (Product product : productsBestSeller) {
						if (count++ > 7)
							break;
				%>
				<%
					if (indexProduct % 4 == 0) {
				%>
				<div class=" top-products">
					<%
						}
					%>


					<div class="col-md-3 md-col">
						<div class="col-md">
							<a href="product.jsp?productId=<%=product.getId()%>"
								class="compare-in"><img src="<%=product.getImage()%>"
								alt="<%=product.getName()%>" /> </a>
							<div class="top-content">
								<h5>
									<a href="product.jsp?productId=<%=product.getId()%>"><%=Utils.getCutName(product.getName())%></a>
								</h5>
								<div class="white">
									<a
										href="CartServlet?command=plus&productId=<%=product.getId()%>"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">MUA
										NGAY</a>
									<p class="dollar">
										<span><%=(int) product.getPrice()%></span><span
											class="in-dollar">VND</span>
									</p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<%
						if (indexProduct % 4 == 3 || indexProduct >= productsBestSeller.size() - 1) {
					%>
					<div class="clearfix"></div>
				</div>
				<%
					}
						indexProduct++;
				%>

				<%
					}
				%>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="container">
			<div class="products">
				<h2 class=" products-in">SẢN PHẨM NỔI BẬT</h2>
				<%
					int indexProductHot = 0;
					int countHot = 0;
					for (Product product : productsHot) {
						if (countHot++ > 7)
							break;
				%>
				<%
					if (indexProductHot % 4 == 0) {
				%>
				<div class=" top-products">
					<%
						}
					%>


					<div class="col-md-3 md-col">
						<div class="col-md">
							<a href="product.jsp?productId=<%=product.getId()%>"
								class="compare-in"><img src="<%=product.getImage()%>"
								alt="<%=product.getName()%>" /> </a>
							<div class="top-content">
								<h5>
									<a href="product.jsp?productId=<%=product.getId()%>"><%=Utils.getCutName(product.getName())%></a>
								</h5>
								<div class="white">
									<a
										href="CartServlet?command=plus&productId=<%=product.getId()%>"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">MUA
										NGAY</a>
									<p class="dollar">
										<span><%=(int) product.getPrice()%></span><span
											class="in-dollar">VND</span>
									</p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<%
						if (indexProductHot % 4 == 3 || indexProductHot >= productsHot.size() - 1) {
					%>
					<div class="clearfix"></div>
				</div>
				<%
					}
						indexProductHot++;
				%>

				<%
					}
				%>
			</div>
		</div>
		<div class="clearfix"></div>
		<!---->
		<div class="content-middle">
			<h3 class="future">THƯƠNG HIỆU</h3>
			<div class="content-middle-in">
				<ul id="flexiselDemo1">
					<%
						for (Brand brand : brands) {
					%>

					<li><img src="<%=brand.getBrandImage()%> "
						style="background-color: black" /></li>
					<%
						}
					%>

				</ul>
				<script type="text/javascript">
					$(window).load(function() {
						$("#flexiselDemo1").flexisel({
							visibleItems : 2,
							animationSpeed : 1000,
							autoPlay : true,
							autoPlaySpeed : 3000,
							pauseOnHover : true,
							enableResponsiveBreakpoints : true,
							responsiveBreakpoints : {
								portrait : {
									changePoint : 480,
									visibleItems : 1
								},
								landscape : {
									changePoint : 640,
									visibleItems : 2
								},
								tablet : {
									changePoint : 768,
									visibleItems : 2
								}
							}
						});

					});
				</script>
				<script type="text/javascript" src="js/jquery.flexisel.js"></script>

			</div>
		</div>
		<!---->
		<div class="content-bottom">
			<h3 class="future">TẤT CẢ SẢN PHẨM</h3>
			<div class="content-bottom-in">
				<ul id="flexiselDemo2">
					<%
						for (Product product : products) {
					%>

					<li><div class="col-md men">
							<a href="product.jsp?productId=<%=product.getId()%>"
								class="compare-in "><img src="<%=product.getImage()%>"
								alt="<%=product.getName()%>" /> </a>
							<div class="top-content bag">
								<h5>
									<a href="product.jsp?productId=<%=product.getId()%>"><%=Utils.getShortName(product.getName())%></a>
								</h5>
								<div class="white">
									<a
										href="CartServlet?command=plus&productId=<%=product.getId()%>"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">MUA
										HANG</a>
									<p class="dollar">
										<span><%=(int) product.getPrice()%></span><span
											class="in-dollar">VND</span>
									</p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div></li>


					<%
						}
					%>
				</ul>
				<script type="text/javascript">
					$(window).load(function() {
						$("#flexiselDemo2").flexisel({
							visibleItems : 4,
							animationSpeed : 1000,
							autoPlay : true,
							autoPlaySpeed : 3000,
							pauseOnHover : true,
							enableResponsiveBreakpoints : true,
							responsiveBreakpoints : {
								portrait : {
									changePoint : 480,
									visibleItems : 1
								},
								landscape : {
									changePoint : 640,
									visibleItems : 2
								},
								tablet : {
									changePoint : 768,
									visibleItems : 3
								}
							}
						});

					});
				</script>
			</div>
		</div>
	</div>
</div>