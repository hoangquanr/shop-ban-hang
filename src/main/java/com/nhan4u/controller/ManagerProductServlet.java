package com.nhan4u.controller;

import com.nhan4u.model.Brand;
import com.nhan4u.model.Category;
import com.nhan4u.model.Product;
import com.nhan4u.service.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/ManagerProductServlet")
public class ManagerProductServlet extends HttpServlet {
	ProductService service = new ProductService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		String command = req.getParameter("command");
		String tenSanPham = req.getParameter("tenSanPham");
		String categoryId = req.getParameter("categoryId");
		String brandId = req.getParameter("brandId");
		String price = req.getParameter("giaSanPham");
		String image = req.getParameter("hinhSanPham");
		String description = req.getParameter("motaSanPham");
		String productId = req.getParameter("productId");
		String url = "";
		try {
			Product product = new Product();
			product.setName(tenSanPham);
			product.setPrice(Double.parseDouble(price));
			product.setImage(image);
			product.setDescription(description);
			Category category = new Category();
			category.setCategoryId(Long.parseLong(categoryId));
			Brand brand = new Brand();
			brand.setBrandId(Long.parseLong(brandId));
			product.setBrand(brand);
			product.setCategory(category);
			switch (command) {
			case "insert":

				product.setId(new Date().getTime());
				service.insert(product);
				url = "admin/manager-product.jsp";
				break;
			case "update":
				product.setId(Long.parseLong(productId));
				service.update(product);
				url = "admin/manager-product.jsp";
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.sendRedirect(url);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		String command = req.getParameter("command");
		String productId = req.getParameter("productId");

		String url = "";
		try {

			switch (command) {

			case "delete":
				Product product = new Product();
				product.setId(Long.parseLong(productId));
				service.delete(product);
				url = "admin/manager-product.jsp";
				break;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		resp.sendRedirect(url);
	}
}
