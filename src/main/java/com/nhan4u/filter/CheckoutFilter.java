package com.nhan4u.filter;

import com.nhan4u.model.Cart;
import com.nhan4u.model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/checkout.jsp")
public class CheckoutFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		// Neu chua dang nhap yeu cau nguoi dung dang nhap
		User user = (User) session.getAttribute("user");
		Cart cart = (Cart) session.getAttribute("cart");
		String error = "";
		String url = "";
		if (cart == null || cart.getItems().size() == 0) {
			error = "Không có sản phẩm trong giỏ";
			url = "index.jsp";
		} else {
			if (user == null) {
				error = "Người dùng chưa đăng nhập";
				url = "login.jsp";
			}
		}
		if (error.equals("")) {
			chain.doFilter(request, response);
		} else {
			resp.sendRedirect(url);
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

}
