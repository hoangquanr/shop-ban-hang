package com.nhan4u.service;

import com.nhan4u.hibernate.util.HibernateUtil;
import com.nhan4u.model.Bill;
import com.nhan4u.model.BillDetail;
import com.nhan4u.model.Category;
import com.nhan4u.model.Product;
import com.nhan4u.tool.SendEmail;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestService {

	public List<Category> getListOfCategories() {

		List<Category> list = new ArrayList<Category>();
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			list = session.createQuery("from Category").list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

public List<Product> getProductBestSeller(){
		
		List<Product> list = new ArrayList<Product>();
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			Query query = session.createQuery("select sum(b.quantity), b.product from BillDetail b group by b.product order by b.price ASC");
			List<Object[]> listResult = query.list();

			for (Object[] aRow : listResult) 
			{
			    Long sum = (long) aRow[0];
			    Product product = (Product) aRow[1];
			    list.add(product);
			}
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	public void addBill() {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			BillDetail item1 = new BillDetail();
			item1.setBillDetailId(21);
			BillDetail item2 = new BillDetail();
			item2.setBillDetailId(2);
			Bill bill = new Bill();
			bill.setBillId(3);
			Set<BillDetail> items = new HashSet<BillDetail>();
			items.add(item1);
			items.add(item2);
			bill.setBillDetails(items);
			session.save(bill);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void sendMail() {
		System.out.println(SendEmail.sendMail("nhantx.1906@gmail.com", "Don hang", "nac danh"));
	}

	public void getCountProductsTest() {
		Session session = HibernateUtil.openSession();
		List<Product> list = new ArrayList<Product>();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			list = session.createQuery("from product").setFirstResult(0).setMaxResults(1).list();
			System.out.println("hien ra di" + list.size() + list.get(0).getName() + "");
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
