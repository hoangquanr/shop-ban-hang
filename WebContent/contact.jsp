<%@page import="com.nhan4u.model.Category"%>
<%@page import="java.util.List"%>
<%@page import="com.nhan4u.service.CategoryService"%>
<%@page import="com.nhan4u.model.Product"%>
<%@page import="com.nhan4u.service.ProductService"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Java Server Page</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 






</script>
<style>
#more {
	display: none;
}
</style>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<link rel="stylesheet" href="css/etalage.css">
<script src="js/jquery.etalage.min.js"></script>
<script>
	jQuery(document)
			.ready(
					function($) {

						$('#etalage')
								.etalage(
										{
											thumb_image_width : 300,
											thumb_image_height : 400,
											source_image_width : 900,
											source_image_height : 1200,
											show_hint : true,
											click_callback : function(
													image_anchor, instance_id) {
												alert('Callback example:\nYou clicked on an image with the anchor: "'
														+ image_anchor
														+ '"\n(in Etalage instance: "'
														+ instance_id + '")');
											}
										});

					});
</script>
<script>
	$(document).ready(function(c) {
		$('.alert-close').on('click', function(c) {
			$('.message').fadeOut('slow', function(c) {
				$('.message').remove();
			});
		});
	});
</script>
<script>
	$(document).ready(function(c) {
		$('.alert-close1').on('click', function(c) {
			$('.message1').fadeOut('slow', function(c) {
				$('.message1').remove();
			});
		});
	});
</script>
</head>
<body>
	<jsp:include page="jsp/header.jsp"></jsp:include>
	<div class="container">
		<div class="contact">
			<h2 class=" contact-in">CONTACT</h2>

			<div class="col-md-6 contact-top">
				<h3>Info</h3>
				<div class="map">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d37494223.23909492!2d103!3d55!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x453c569a896724fb%3A0x1409fdf86611f613!2sRussia!5e0!3m2!1sen!2sin!4v1415776049771"></iframe>
				</div>

				<p>At vero eos et accusamus et iusto odio dignissimos ducimus
					qui blanditiis praesentium voluptatum deleniti atque corrupti quos
					dolores et quas</p>
				<p>Et harum quidem rerum facilis est et expedita distinctio. Nam
					libero tempore, cum soluta nobis est eligendi optio cumque nihil
					impedit quo minus id</p>
				<ul class="social ">
					<li><span><i> </i>124 Avenue Street, Los
							angeles,California </span></li>
					<li><span><i class="down"> </i>+ 00 123 456 7890</span></li>
					<li><a href="mailto:info@example.com"><i class="mes">
						</i>info@example.com</a></li>
				</ul>
			</div>
			<div class="col-md-6 contact-top">
				<h3>Want to work with me?</h3>
				<div>
					<span>Your Name </span> <input type="text" value="">
				</div>
				<div>
					<span>Your Email </span> <input type="text" value="">
				</div>
				<div>
					<span>Subject</span> <input type="text" value="">
				</div>
				<div>
					<span>Your Message</span>
					<textarea> </textarea>
				</div>
				<input type="submit" value="SEND">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<jsp:include page="jsp/footer.jsp"></jsp:include>
	<script>
		function more() {
			var dots = document.getElementById("dots");
			var moreText = document.getElementById("more");
			var btnText = document.getElementById("myBtn");

			if (dots.style.display === "none") {
				dots.style.display = "inline";
				btnText.innerHTML = "Xem them";
				moreText.style.display = "none";
			} else {
				dots.style.display = "none";
				btnText.innerHTML = "An di";
				moreText.style.display = "inline";
			}
		}
	</script>
</body>
</html>
