package com.nhan4u.filter;

import com.nhan4u.tool.Utils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet Filter implementation class ValidateBillFilter
 */
@WebFilter("/ManagerBillServlet")
public class ValidateBillFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public ValidateBillFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String address = request.getParameter("diachi");
		String phone = request.getParameter("phone");
		String billId = request.getParameter("billId");
		String error = "";
		if (address == null || phone == null || address.equals("") || phone.equals("")) {
			error = "Địa chỉ không đúng";
		}
		if (!Utils.checkNumberPhone(phone)) {
			if (error.equals(""))
				error = "Số điện thoại không đúng";
			else
				error += ", Số điện thoại không đúng";
		}
		request.setAttribute("errorBill", error);
		if (error.equals("")) {
			chain.doFilter(request, response);
		} else {
			resp.sendRedirect("admin/update-bill.jsp?billId=" + billId);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
