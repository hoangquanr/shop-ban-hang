<%@page import="com.nhan4u.model.Item"%>
<%@page import="java.util.Map"%>
<%@page import="com.nhan4u.model.Cart"%>
<%@page import="com.nhan4u.model.User"%>
<%@page import="java.util.List"%>
<%@page import="com.nhan4u.model.Category"%>
<%@page import="com.nhan4u.service.CategoryService"%>
<%@ page pageEncoding="utf-8"%>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
	src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=1991454701172565&autoLogAppEvents=1"></script>

<%
	CategoryService service = new CategoryService();
	User user = null;
	if (session.getAttribute("user") != null)
		user = (User) session.getAttribute("user");
	// Tao session va lay cart
	Cart cart = (Cart) session.getAttribute("cart");
	if (cart == null) {
		cart = new Cart();
		session.setAttribute("cart", cart);
	}
%>
<div class="header">
	<div class="header-top">
		<div class="container">
			<div class="header-top-in">
				<div class="logo">
					<a href="index.jsp"><img src="images/logo.png" alt=" "></a>
				</div>
				<div class="header-in">
					<ul class="icon1 sub-icon1">
						<%
							if (user != null) {
						%>

						<li><a
							href="/ShopQuanAo/LogoutServlet?logout=true&command=user"><%=user.getEmail()%></a></li>
						<%
							} else {
						%>
						<li><a href="login.jsp">TÀI KHOẢN</a></li>
						<%
							}
						%>
						<li><a href="checkout.jsp">THANH TOÁN</a></li>
						<li><div class="cart">
								<a href="#" class="cart-in"> </a> <span> <%=cart.countItem()%></span>
							</div>
							<ul class="sub-icon1 list">
								<h3>
									Sản phẩm trong giỏ(<%=cart.countItem()%>)
								</h3>
								<div class="shopping_cart">
									<%
										for (Map.Entry<Long, Item> item : cart.getItems().entrySet()) {
									%>
									<div class="cart_box">
										<div class="message">
											<div class="alert-close"></div>
											<div class="list_img">
												<img src="<%=item.getValue().getProduct().getImage()%>"
													class="img-responsive"
													alt="<%=item.getValue().getProduct().getImage()%>">
											</div>
											<div class="list_desc">
												<h4>
													<a
														href="CartServlet?command=remove&productId=<%=item.getValue().getProduct().getId()%>"><%=item.getValue().getProduct().getName()%></a>
												</h4>
												<%=item.getValue().getQuantity()%>
												x<span class="actual"> <%=item.getValue().getProduct().getPrice()%></span>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<%
										}
									%>
								</div>
								<div class="total">
									<div class="total_left">Tổng tiền :</div>
									<div class="total_right"><%=cart.totalCart()%>VND
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="login_buttons">
									<div class="check_button">
										<a href="checkout.jsp">Thanh toán</a>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</ul></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="h_menu4">
				<a class="toggleMenu" href="#">Menu</a>
				<ul class="nav">
					<li
						class="${pageContext.request.requestURI eq '/ShopQuanAo/index.jsp' ? ' active' : ''}"><a
						href="index.jsp"><i> </i>Trang chủ</a></li>
					<li
						class="${pageContext.request.requestURI eq '/ShopQuanAo/products.jsp' ? ' active' : ''}"><a
						href="">Sản phẩm</a>
						<ul class="drop">
							<%
								List<Category> categories = service.getListOfCategories();
								for (Category category : categories) {
							%>
							<li><a
								href="products.jsp?categoryId=<%=category.getCategoryId()%>&pages=1"><%=category.getCategoryName()%></a></li>
							<%
								}
							%>
						</ul></li>
					<li
						class="${pageContext.request.requestURI eq '/ShopQuanAo/contact.jsp' ? ' active' : ''}"><a
						href="contact.jsp">Liên hệ</a></li>
				</ul>
				<script type="text/javascript" src="js/nav.js"></script>
			</div>
		</div>
	</div>
	<div class="header-bottom-in">
		<div class="container">
			<div class="header-bottom-on">
				<p class="wel">
					<a href="#">Sản phẩm hoàn toàn từ thiên nhiên.</a>
				</p>
				<div class="header-can">
					<ul class="social-in">
						<li><a href="#"><i> </i></a></li>
						<li><a href="#"><i class="facebook"> </i></a></li>
						<li><a href="#"><i class="twitter"> </i></a></li>
						<li><a href="#"><i class="skype"> </i></a></li>
					</ul>
					<div class="search">
						<form>
							<input type="text" value="Search" onfocus="this.value = '';"
								onblur="if (this.value == '') {this.value = '';}"> <input
								type="submit" value="">
						</form>
					</div>

					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>