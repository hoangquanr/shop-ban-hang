<%@page import="com.nhan4u.tool.Utils"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.nhan4u.model.Cart"%>
<%@page import="com.nhan4u.model.Product"%>
<%@page import="com.nhan4u.service.ProductService"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Java Server Page</title>
<jsp:include page="jsp/head.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="jsp/header.jsp"></jsp:include>
	<%
		ProductService service = new ProductService();
		String category_id = "";
		if (request.getParameter("categoryId") != null) {
			category_id = request.getParameter("categoryId");
		}
		int pages = 1, first = 0, max = 0, total = 0;
		if (request.getParameter("pages") != null)
			pages = Integer.parseInt(request.getParameter("pages"));
		total = service.countProductByCategory(Long.valueOf(category_id));
		if (total < 8) {
			first = 0;
			max = total;
		} else {
			first = (pages - 1) * 8;
			max = 8;
		}
		List<Product> products = service.getListProductByPage(Long.valueOf(category_id), first, max);
	%>
	<div class="container">
		<div class="products">
			<h2 class=" products-in">DANH SÁCH SẢN PHẨM</h2>
			<%
				int indexProduct = 0;
				for (Product product : products) {
			%>
			<%
				if (indexProduct % 4 == 0) {
			%>
			<div class=" top-products">
				<%
					}
				%>


				<div class="col-md-3 md-col">
					<div class="col-md">
						<a href="product.jsp?productId=<%=product.getId()%>"
							class="compare-in"><img src="<%=product.getImage()%>"
							alt="<%=product.getName()%>" /> </a>
						<div class="top-content">
							<h5>
								<a href="product.jsp?productId=<%=product.getId()%>"><%=Utils.getCutName(product.getName())%></a>
							</h5>
							<div class="white">
								<a
									href="CartServlet?command=plus&productId=<%=product.getId()%>"
									class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">MUA
									NGAY</a>
								<p class="dollar">
									<span><%=(int) product.getPrice()%></span><span
										class="in-dollar">VNĐ</span>
								</p>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>

				<%
					if (indexProduct % 4 == 3 || indexProduct >= products.size() - 1) {
				%>
				<div class="clearfix"></div>
			</div>
			<%
				}
					indexProduct++;
			%>

			<%
				}
			%>
		</div>
		<div class="clearfix"></div>
		<ul class="start">
			<%
				int prevpage = 0;
				if (pages == 1)
					prevpage = total / 8 + 1;
				else
					prevpage = pages - 1;
			%>
			<li><a
				href="products.jsp?categoryId=<%=category_id%>&pages=<%=prevpage%>"><i></i></a></li>
			<%
				for (int i = 1; i <= total / 8 + 1; i++) {
			%>
			<li class="arrow"><a
				href="products.jsp?categoryId=<%=category_id%>&pages=<%=i%>"><%=i%></a></li>
			<%
				}
			%>
			<%
				int nextpage = 0;
				if (pages == total / 8 + 1)
					nextpage = 1;
				else
					nextpage = pages + 1;
			%>
			<li><a
				href="products.jsp?categoryId=<%=category_id%>&pages=<%=nextpage%>"><i
					class="next"> </i></a></li>

		</ul>
	</div>

	<jsp:include page="jsp/footer.jsp"></jsp:include>
</body>
</html>