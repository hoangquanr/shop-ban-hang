<%@page import="com.nhan4u.model.Category"%>
<%@page import="java.util.List"%>
<%@page import="com.nhan4u.service.CategoryService"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Quản lý danh mục</title>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<%
		CategoryService service = new CategoryService();
		List<Category> categories = service.getListOfCategories();
	%>
	<div id="wrapper">
		<jsp:include page="jsp/nav.jsp"></jsp:include>
		<div id="page-wrapper">

			<div class="container-fluid">

				<div class="row">
					<h2>Quản lý danh mục</h2>
					<a href="insert-category.jsp">Thêm thư mục</a>
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
								<tr>
									<th width="30px">STT</th>
									<th>Mã danh mục</th>
									<th>Tên danh mục</th>
									<th width="90px">Tùy chọn</th>
								</tr>
							</thead>
							<tbody>
								<%
									int count = 0;
									for (Category category : categories) {
										count++;
								%>
								<tr>

									<td><%=count%></td>
									<td><%=category.getCategoryId()%></td>
									<td><%=category.getCategoryName()%></td>
									<td>
										<center>
											<a
												href="/ShopQuanAo/admin/update-category.jsp?categoryId=<%=category.getCategoryId()%>">Sửa</a>
											| <a
												href="/ShopQuanAo/ManagerCategoryServlet?command=delete&categoryId=<%=category.getCategoryId()%>">Xóa</a>
										</center>
									</td>

								</tr>
								<%
									}
								%>
							</tbody>
						</table>
					</div>

				</div>
				<!-- /.row -->

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Morris Charts JavaScript -->
	<script src="js/plugins/morris/raphael.min.js"></script>
	<script src="js/plugins/morris/morris.min.js"></script>
	<script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>