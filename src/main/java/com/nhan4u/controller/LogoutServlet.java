package com.nhan4u.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String command = req.getParameter("command");
		String url = "";
		if (command.equals("user")) {
			req.getSession().removeAttribute("user");
			url = "index.jsp";
		} else if (command.equals("admin")) {
			req.getSession().removeAttribute("useradmin");
			url = "admin/login.jsp";
		}
		resp.sendRedirect(url);
	}

}
