<%@page import="com.nhan4u.model.User"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Java Server Page</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="css/plugins/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="row">
		<div class="col-lg-4 text-center"></div>
		<div class="col-lg-4 text-center">
			<div class="panel panel-default">
				<div class="panel-body">
					<h2>Login</h2>
					<form action="/ShopQuanAo/LoginServlet" method="post">
						<%
							if (request.getAttribute("errorLogin") != null) {
						%>
						<span style="color: red"><%=request.getAttribute("errorLogin")%></span>
						<%
							}
						%>
						<div class="form-group input-group">
							<span class="input-group-addon">Username</span> <input
								type="text" class="form-control" name="email">
						</div>
						<div class="form-group input-group">
							<span class="input-group-addon">Password</span> <input
								type="password" class="form-control" name="password">
						</div>
						<input type="hidden" name="command" value="admin">
						<button type="submit" class="btn btn-default">Dang nhap</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-4 text-center"></div>
	</div>

</body>
</html>