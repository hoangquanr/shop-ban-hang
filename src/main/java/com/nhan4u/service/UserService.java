package com.nhan4u.service;

import com.nhan4u.hibernate.util.HibernateUtil;
import com.nhan4u.model.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserService {
	public boolean checkEmailAddress(String email) {

		Session session = HibernateUtil.openSession();
		boolean flag = false;
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			int result = session.createQuery("from User where user_email='"+email+"'").getResultList().size();
			if(result > 0) {
				flag = true;
			}
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flag;
	}
	
	public boolean register(User user){
	     Session session = HibernateUtil.openSession();
	     if(checkEmailAddress(user.getEmail())) return false;   
	     Transaction tx = null;
	     try {
	         tx = session.getTransaction();
	         tx.begin();
	         session.saveOrUpdate(user);
	         tx.commit();
	     } catch (Exception e) {
	         if (tx != null) {
	             tx.rollback();
	         }
	         e.printStackTrace();
	     } finally {
	         session.close();
	     }  
	     return true;
	}
	
	public boolean authenticateUser(String userEmail, String password) {
        User user = getUserByUserEmail(userEmail);          
        if(user!=null && user.getEmail().equals(userEmail) && user.getPassword().equals(password)){
            return true;
        }else{ 
            return false;
        }
    }
 
    public User getUserByUserEmail(String userEmail) {
        Session session = HibernateUtil.openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            Query query = session.createQuery("from User where user_email = '"+userEmail+"'");
            user = (User)query.uniqueResult();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }
}
