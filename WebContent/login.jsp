<%@page import="com.nhan4u.model.User"%>
<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Login</title>
<jsp:include page="jsp/head.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="jsp/header.jsp"></jsp:include>
	<div class="container">
		<div class="account">
			<h2 class="account-in">Login</h2>
			<form action="LoginServlet" method="post">
				<%
					if (request.getAttribute("errorLogin") != null) {
				%>
				<span style="color: red"><%=request.getAttribute("errorLogin")%></span>
				<%
					}
				%>
				<div>
					<span class="mail">Email*</span> <input type="text" name="email">
				</div>
				<div>
					<span class="word">Password*</span> <input type="password"
						name="password">
				</div>
				<input type="hidden" name="command" value="user">
				<div>
					<input type="submit" value="Login"> <input type=submit
						value="Register account" formaction="register.jsp"
						formmethod="post">
				</div>
			</form>
		</div>
	</div>
	<jsp:include page="jsp/footer.jsp"></jsp:include>
</body>
</html>