package com.nhan4u.controller;

import com.nhan4u.model.User;
import com.nhan4u.service.UserService;
import com.nhan4u.tool.MD5;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	UserService service = new UserService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String command = req.getParameter("command");
		String userEmail = req.getParameter("email");
		String password = MD5.encryption(req.getParameter("password"));
		boolean result = service.authenticateUser(userEmail, password);
		User user = service.getUserByUserEmail(userEmail);
		String url = "";
		if (result == true) {
			switch (command) {
			case "admin":
				if (user.isRole() == true) {
					req.getSession().setAttribute("useradmin", user);
					url = "admin/index.jsp";
				} else {
					url = "admin/login.jsp";
				}

				break;
			default:
				req.getSession().setAttribute("user", user);
				url = "index.jsp";
				break;
			}
		} else {
			switch (command) {
			case "admin":
				url = "admin/login.jsp";
				break;
			default:
				url = "login.jsp";
				break;
			}

		}

		resp.sendRedirect(url);

	}
}
