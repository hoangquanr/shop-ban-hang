package com.nhan4u.service;

import com.nhan4u.hibernate.util.HibernateUtil;
import com.nhan4u.model.Bill;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class BillService {

	public void insertBill(Bill bill) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			session.save(bill);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public double total() {
		double total = 0.0;
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			total = (double)session.createQuery("select sum(b.total) from Bill b").getSingleResult();
			System.out.println(total + "KET QUAAAAAAAA");
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return total;
	}
	
	public long totalNewBill() {
		long total = 0;
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			total = (long)session.createQuery("select count(*) from Bill b where b.status = -1").getSingleResult();
			System.out.println(total + "KET QUAAAAAAAA");
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return total;
	}
	
	
	public void update(Bill bill) {
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			session.update(bill);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	

	public List<Bill> getListOfBill() {

		List<Bill> list = new ArrayList<Bill>();
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			list = session.createQuery("from Bill order by status ASC").list();
			;

			System.out.println(list.size() + "SIZE cua bill");
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}
	
	public List<Bill> getListOfNewBill() {

		List<Bill> list = new ArrayList<Bill>();
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			list = session.createQuery("from Bill where status=-1").list();
			;

			System.out.println(list.size() + "SIZE cua bill");
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	public Bill getBillById(long bill_id) {
		Bill bill = null;
		Session session = HibernateUtil.openSession();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			bill = (Bill) session.createQuery("from Bill where bill_id=" + bill_id).getSingleResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return bill;
	}

}
