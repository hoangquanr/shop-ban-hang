package com.nhan4u.model;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {

	@Id
	@Column(name = "product_id")
	private long id;
	@Column(name = "product_name")
	private String name;
	@Column(name = "product_image")
	private String image;
	@Column(name = "product_price")
	private double price;
	@Column(name = "product_description")
	private String description;
	 @ManyToOne
	 @JoinColumn(name = "category_id")
	private Category category;
	 @ManyToOne
	 @JoinColumn(name = "brand_id")
	private Brand brand;


	@Transient
	private String[] images;

	public Product() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		images = image.split("\\,");
		return images[0];
	}

	public String[] getListImages() {
		images = image.split("\\,");
		return images;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Product(long id, String name, String image, double price, String description, Category category,
			Brand brand) {
		super();
		this.id = id;
		this.name = name;
		this.image = image;
		this.price = price;
		this.description = description;
		this.category = category;
		this.brand = brand;
	}


}
