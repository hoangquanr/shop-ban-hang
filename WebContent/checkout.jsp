<%@page import="com.nhan4u.model.User"%>
<%@page import="com.nhan4u.service.TestService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<title>Java Server Page</title>
<jsp:include page="jsp/head.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="jsp/header.jsp"></jsp:include>

	<div class="container">
		<div class="account">
			<h2 class="account-in">Register</h2>
			<form action="CheckoutServlet" method="post">
				<%
					if (request.getAttribute("error") != null) {
				%>
				<span style="color: red"><%=request.getAttribute("error")%></span>
				<%
					}
				%>
				<div>
					<span class="mail">Address *</span> <input type="text"
						name="address">
				</div>
				<div>
					<span class="mail">Phone *</span> <input type="text" name="phone">
				</div>
				<div>
					<span class="word">Payment *</span> <select name="payment">
						<option value="Bank transfer">Bank transfer</option>
						<option value="Live">Live</option>
					</select>
				</div>
				<input type="submit" value="Checkout">
			</form>
		</div>
	</div>

	<jsp:include page="jsp/footer.jsp"></jsp:include>
</body>
</html>