package com.nhan4u.filter;

import com.nhan4u.service.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet Filter implementation class ValidateRegister
 */
@WebFilter("/RegisterServlet")
public class ValidateRegisterFilter implements Filter {

	UserService service = new UserService();

	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String error = "";
		String userEmail = req.getParameter("email");
		String password = req.getParameter("password");
		if (userEmail == null || userEmail.equals(""))
			error = "Email chưa nhập";
		if (password == null || password.equals("")) {
			if (error.equals(""))
				error = "Password chưa nhập";
			else
				error += ", Password chưa nhập";
		}
		boolean result = service.checkEmailAddress(userEmail);
		if (result == true)
			error = "Email đã tồn tại";
		request.setAttribute("errorRegister", error);
		if (error.equals(""))
			chain.doFilter(request, response);
		else {
			req.getRequestDispatcher("register.jsp").forward(req, resp);
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
