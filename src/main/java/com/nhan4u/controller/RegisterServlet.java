package com.nhan4u.controller;

import com.nhan4u.model.User;
import com.nhan4u.service.UserService;
import com.nhan4u.tool.MD5;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {

	UserService service = new UserService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		User user = new User();
		user.setId(new Date().getTime());
		user.setEmail(req.getParameter("email"));
		user.setPassword(MD5.encryption(req.getParameter("password")));
		user.setRole(false);
		System.out.println(user);
		String url = "";
		service.register(user);
		HttpSession session = req.getSession();
		session.setAttribute("user", user);
		url = "/index.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
		rd.forward(req, resp);

	}
}
